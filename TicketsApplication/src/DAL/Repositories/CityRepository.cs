﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class CityRepository : IRepository<City>
    {
        DataContext db;

        public CityRepository(DataContext context)
        {
           db = context;
        }

        public List<City> GetAll()=> db.Cities.ToList();

        public City Get(int id) => db.Cities.Where(o => o.ID == id).FirstOrDefault();

        public void Create(City city)=> db.Cities.Add(city);

        public void Update(City city) => db.Entry(city).State = EntityState.Modified;

        public void Delete(int id)
        {
            City city = db.Cities.Where(o => o.ID == id).FirstOrDefault();
            if (city != null)
                db.Cities.Remove(city);
        }
    }


}
