﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class EventRepository : IRepository<Event>
    {
        DataContext db;

        public EventRepository(DataContext context)
        {
            db = context;
        }

        public List<Event> GetAll()=> db.Events.ToList();

        public Event Get(int id)=>db.Events.Where(o => o.ID == id).FirstOrDefault();

        public void Create(Event _event)=> db.Events.Add(_event);

        public void Update(Event _event)=> db.Entry(_event).State = EntityState.Modified;

        public void Delete(int id)
        {
            Event _event = db.Events.Where(o => o.ID == id).FirstOrDefault();
            if (_event != null)
                db.Events.Remove(_event);
        }
    }
}
