﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class OrderRepository : IRepository<Order>
    {
        DataContext db;

        public OrderRepository(DataContext context)
        {
            db = context;
        }

        public List<Order> GetAll()=> db.Orders.ToList();

        public Order Get(int id) => db.Orders.Where(o => o.ID == id).FirstOrDefault();

        public void Create(Order order)=> db.Orders.Add(order);

        public void Update(Order order)=> db.Entry(order).State = EntityState.Modified;

        public void Delete(int id)
        {
            Order order = db.Orders.Where(o => o.ID == id).FirstOrDefault();
            if (order != null)
                db.Orders.Remove(order);
        }
    }
}
