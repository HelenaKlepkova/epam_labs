﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Hash;
using DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class PasswordRepository : IRepository<Password>
    {
        DataContext db;

        public PasswordRepository(DataContext context)
        {
            db = context;
        }

        public List<Password> GetAll()=> db.Passwords.ToList();

        public Password Get(int id)=> db.Passwords.Where(o => o.ID == id).FirstOrDefault();

        public void Create(Password password)
        {
            password.Password_ = Hash.Hash.HashPassword(password.Password_);
            db.Passwords.Add(password);
        }

        public void Update(Password password)=> db.Entry(password).State = EntityState.Modified;
     
        public void Delete(int id)
        {
           Password password = db.Passwords.Where(o => o.ID == id).FirstOrDefault();
            if (password != null)
                db.Passwords.Remove(password);
        }
    }
}
