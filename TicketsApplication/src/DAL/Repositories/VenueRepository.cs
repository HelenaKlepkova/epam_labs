﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class VenueRepository : IRepository<Venue>
    {
        DataContext db;

        public VenueRepository(DataContext context)
        {
            db = context;
        }

        public List<Venue> GetAll()=> db.Venues.ToList();

        public Venue Get(int id)=> db.Venues.Where(o => o.ID == id).FirstOrDefault();

        public void Create(Venue venue)=> db.Venues.Add(venue);

        public void Update(Venue venue)=> db.Entry(venue).State = EntityState.Modified;

        public void Delete(int id)
        {
            Venue venue = db.Venues.Where(o => o.ID == id).FirstOrDefault();
            if (venue != null)
                db.Venues.Remove(venue);
        }
    }
}
