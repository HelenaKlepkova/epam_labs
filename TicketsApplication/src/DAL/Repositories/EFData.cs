﻿//using DAL.EF;
using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class EFData : IData
    {
        DataContext db;
        CityRepository cityRepository;
        OrderRepository orderRepository;
        TicketRepository ticketRepository;
        UserRepository userRepository;
        VenueRepository venueRepository;
        EventRepository eventRepository;
        PasswordRepository passwordRepository;
        RoleRepository roleRepository;

        public EFData(DataContext db)
        {
               this.db = db;
        }

        public IRepository<City> Cities
        {
            get
            {
                if (cityRepository == null)
                    cityRepository = new CityRepository(db);
                return cityRepository;
            }
        }

        public IRepository<Role> Roles
        {
            get
            {
                if (roleRepository == null)
                    roleRepository = new RoleRepository(db);
                return roleRepository;
            }
        }

        public IRepository<Order> Orders
        {
            get
            {
                if (orderRepository == null)
                    orderRepository = new OrderRepository(db);
                return orderRepository;
            }
        }
        public IRepository<Event> Events
        {
            get
            {
                if (eventRepository == null)
                    eventRepository = new EventRepository(db);
                return eventRepository;
            }
        }
        public IRepository<Password> Passwords
        {
            get
            {
                if (passwordRepository == null)
                    passwordRepository = new PasswordRepository(db);
                return passwordRepository;
            }
        }

        public IRepository<Ticket> Tickets
        {
            get
            {
                if (ticketRepository == null)
                    ticketRepository = new TicketRepository(db);
                return ticketRepository;
            }
        }
        public IRepository<User> Users
        {
            get
            {
                if (userRepository == null)
                    userRepository = new UserRepository(db);
                return userRepository;
            }
        }
        public IRepository<Venue> Venues
        {
            get
            {
                if (venueRepository == null)
                    venueRepository = new VenueRepository(db);
                return venueRepository;
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }

    }
}
