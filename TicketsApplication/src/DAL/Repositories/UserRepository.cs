﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        DataContext db;

        public UserRepository(DataContext context)
        {
            db = context;
        }

        public List<User> GetAll()=> db.Users.ToList();

        public User Get(int id)=> db.Users.Where(o => o.ID == id).FirstOrDefault();

        public void Create(User user)=> db.Users.Add(user);

        public void Update(User user) => db.Entry(user).State = EntityState.Modified;

        public void Delete(int id)
        {
           User user = db.Users.Where(o => o.ID == id).FirstOrDefault();
            if (user != null)
               db.Users.Remove(user);
        }
    }
}
