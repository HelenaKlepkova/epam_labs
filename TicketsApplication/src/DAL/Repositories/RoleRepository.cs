﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class RoleRepository : IRepository<Role>
    {
        DataContext db;

        public RoleRepository(DataContext context)
        {
            db = context;
        }

        public List<Role> GetAll() => db.Roles.ToList();

        public Role Get(int id) => db.Roles.Where(o => o.ID == id).FirstOrDefault();

        public void Create(Role role) => db.Roles.Add(role);

        public void Update(Role role) => db.Entry(role).State = EntityState.Modified;

        public void Delete(int id)
        {
            Role role = db.Roles.Where(o => o.ID == id).FirstOrDefault();
            if (role != null)
                db.Roles.Remove(role);
        }
    }
}
