﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class TicketRepository : IRepository<Ticket>
    {
        DataContext db;

        public TicketRepository(DataContext context)
        {
            db = context;
        }

        public List<Ticket> GetAll()=> db.Tickets.ToList();

        public Ticket Get(int id)=>db.Tickets.Where(o => o.ID == id).FirstOrDefault();

        public void Create(Ticket ticket)=> db.Tickets.Add(ticket);

        public void Update(Ticket ticket)=> db.Entry(ticket).State = EntityState.Modified;

        public void Delete(int id)
        {
            Ticket ticket = db.Tickets.Where(o => o.ID == id).FirstOrDefault();
            if (ticket != null)
                db.Tickets.Remove(ticket);
        }
    }
}
