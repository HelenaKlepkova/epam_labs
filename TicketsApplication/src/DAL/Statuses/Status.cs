﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Statuses
{
    public static class Status
    {
        public enum status { WaitingConfirmation = 1, Sold = 2, Rejection = 3 };
    }
}
