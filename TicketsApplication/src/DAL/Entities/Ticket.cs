﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Ticket
    {
        public int ID { get; set; }
        public int EventID { get; set; }
        public decimal Price { get; set; }
        public int SellerID { get; set; }
        public int BuyerID { get; set; }
        public bool Availability { get; set; }
        public Event Event { get; set; }
        public Order Order { get; set; }
        public User User { get; set; }
    }
}
