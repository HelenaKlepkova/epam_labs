﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class City
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<Venue> Venues { get; set; }
    }
}
