﻿
using DAL.Statuses;

namespace DAL.Entities
{
    public class Order
    {
        public int ID { get; set; }
        public int TicketID { get; set; }
        public Status.status Status { get; set; }
        public int BuyerID { get; set; }
        public int SellerID { get; set; }
        public string TrackNumber { get; set; }
        public string RejectionReason { get; set; }
        public Ticket Ticket { get; set; }

    }
}

