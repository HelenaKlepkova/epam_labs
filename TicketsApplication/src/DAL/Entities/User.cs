﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class User
    {
        public int ID { get; set; }
        [MaxLength(15)]
        public string Nick { get; set; }
        [MaxLength(15)]
        public string FirstName { get; set; }
        [MaxLength(30)]
        public string LastName { get; set; }
        public string Localization { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int PasswordID { get; set; }
        public int RoleID { get; set; }
        public List<Ticket> Ticket { get; set; }
        public Role Role { get; set; }
    }
}
