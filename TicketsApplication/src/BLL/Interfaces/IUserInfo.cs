﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserInfo
    {
        UserDTO GetUser(string nick);
        void change_password(string nick, string newPassword);
        void change_nick(string oldNick, string newNick);
        void change_phone(string nick, string newPhone);
        void change_address(string nick, string newAddress);
    }
}
