﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IAccount
    {
        UserDTO CreateUser(string nick, string firstName, string lastName, int passwordId, string address, string localization, string phone);
        UserDTO GetUser(string nick);
        bool IsExsists(string name);
        bool Register(UserDTO user, string password);
        int GetPasswordID(string password);
        int NewPasswordID();
        int GetPasswordIdInUsers(string nick);
        string GetPassword(int id);
        string GetRoleNameForUserNick(string nick);
    }
}
