﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ITicketService
    {
        ICollection<KeyValuePair<decimal, int>> GetTicketCountDTO(int eventId);
        EventDTO GetEventForTicket(int eventId);
        List<TicketDTO> TicketsForEvent(int eventID, decimal price);
        List<TicketDTO> TicketsForUser(UserDTO user);
        List<TicketDTO> TicketOrderUser(UserDTO user);
        List<TicketDTO> TicketStart(UserDTO user);
        void update_tickets(List<TicketDTO> tickets);
        void buy_tickets(List<TicketDTO> tickets);
        void update_order_info(int ticketID, int newStatus, string message = null, int sellerID = 0, int buyerID = 0);
    }
}
