﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IVenueOptions
    {
        void create_venue(VenueDTO venue);
        void delete_venue(int id);
        VenueDTO GetVenue(int id);
        void update_venue(VenueDTO venue);
        List<CityDTO> GetCities();
        List<VenueDTO> GetVenues();
    }
}
