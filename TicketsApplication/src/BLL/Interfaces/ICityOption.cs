﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ICityOption
    {
        void create_city(CityDTO city);
        void delete_city(int cityID);
        CityDTO GetCity(int id);
        void update_city(CityDTO city);
        List<CityDTO> GetCities();
    }
}
