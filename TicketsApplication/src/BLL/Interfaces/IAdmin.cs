﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IAdmin
    {
        List<UserDTO> GetUsers();
        void change_role(int userID, string target);
    }
}
