﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IEventOption
    {
        EventDTO GetEvent(int id);
        int GetIDLastEvent();
        void delete_event(int id);
        void add_event(EventDTO ev);
        List<EventDTO> GetEvents(int cityID);
        void update_event(EventDTO ev);
        string Banner(int id);
        List<CityDTO> GetCities();
        List<VenueDTO> GetVenues();
        void add_tickets(int count, int eventID);
    }
}
