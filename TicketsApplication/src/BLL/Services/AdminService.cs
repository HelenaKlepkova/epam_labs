﻿using BLL.DTO;
using BLL.Interfaces;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL.Entities;

namespace BLL.Services
{
    public class AdminService : IAdmin
    {
        EFData data;

        public AdminService(EFData data)
        {
            this.data = data;
        }  

        #region adminAdd/deleteOption

        public List<UserDTO> GetUsers() => Mapper.Map<List<UserDTO>>(data.Users.GetAll());

        public void change_role(int userID, string target)
        {
            User us = data.Users.Get(userID);
            if (us != null)
            {
                if (target == "admin")
                    us.RoleID = 1;
                if (target == "user")
                    us.RoleID = 2;
                data.Users.Update(us);
                data.Save();
            }
        }
 
        #endregion


    }
}
