﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Statuses;

namespace BLL.Services
{
    public class TicketService : ITicketService
    {
        EFData Data;
        
        public TicketService(EFData data)
        {
            Data = data;
        }

        #region startTicket
        public List<TicketDTO> TicketStart(UserDTO user)
        {
            List<TicketDTO> tickets = Mapper.Map<List<TicketDTO>>(Data.Tickets.GetAll()
           .Where(o => o.SellerID == user.ID && o.Availability == false));
            List<Order> orders = Data.Orders.GetAll();
            foreach(var i in orders)
            {
                if (i.Status == (Status.status)3 || i.Status == (Status.status)1 || i.Status==0)
                {
                    TicketDTO obj = tickets.Find(o => o.ID == i.TicketID);
                    tickets.Remove(obj);
                }
            }
            List<EventDTO> events = Mapper.Map<List<EventDTO>>(Data.Events.GetAll());
            foreach (var i in tickets)
            {
                i.Event = events.Find(o => o.ID == i.EventID).Name;
            }
            return tickets;
        }

     
        #endregion

        #region sellTickets
        public ICollection<KeyValuePair<decimal, int>> GetTicketCountDTO(int eventId)
        {
            TicketCountDTO tcd = new TicketCountDTO();
            tcd.PriceAndCount = AddCountWithPrice(eventId);
            return (tcd.PriceAndCount);
        }


        private ICollection<KeyValuePair<decimal, int>> AddCountWithPrice(int eventId)
        {
            List<TicketDTO> ticketsCopy = TicketsForEvent(eventId);
            ICollection<KeyValuePair<decimal, int>> priceAndCount =
          new Dictionary<decimal, int>();
            while (ticketsCopy.Count > 0)
            {
                decimal price = ticketsCopy[0].Price;
                int count = ticketsCopy.FindAll(o => o.Price == price).Count();
                priceAndCount.Add(new KeyValuePair<decimal, int>(price, count));
                ticketsCopy.RemoveAll(o => o.Price == price);
            }
            return priceAndCount;
        }
     
        private List<TicketDTO> TicketsForEvent(int eventID) => (Tickets()
    .FindAll(o => (o.EventID == eventID) && (o.Availability)));

        #endregion

        #region buyTickets

        public void buy_tickets(List<TicketDTO> tickets)
        {
            update_tickets(tickets);
            foreach (var i in tickets)
            {
                Order order = new Order();
                order.TicketID = i.ID;
                order.Status = (Status.status)1;
                order.BuyerID = i.BuyerID;
                order.SellerID = i.SellerID;
                Data.Orders.Create(order);
            }
            Data.Save();
        }

        public void update_tickets(List<TicketDTO> tickets)
        {
            foreach (var i in tickets)
            {
                Ticket t = Data.Tickets.Get(i.ID);
                if (t != null)
                {
                    t.Availability = i.Availability;
                    t.BuyerID = i.BuyerID;
                    Data.Tickets.Update(t);
                }
            }
            Data.Save();
        }

        #endregion

        #region заказы у пользователя
        public List<TicketDTO> TicketsForUser(UserDTO user)
        {
            List<Order> ordersUser = Data.Orders.GetAll()
                .FindAll(o => o.SellerID == user.ID && o.Status == (Status.status)1);
            List<User> users = Data.Users.GetAll();
            List<TicketDTO> allTickets = Tickets();
            List<TicketDTO> ticketsForUser = new List<TicketDTO>();
            foreach (var i in ordersUser)
            {
                int ti = i.TicketID;
                TicketDTO ticket = allTickets.Find(o => o.ID == ti);
                ticket.OrderStatus = i.Status.ToString();
                ticket.BuyerID = i.BuyerID;
                ticketsForUser.Add(ticket);
            }
            List<Event> events = Data.Events.GetAll();
            foreach (var i in ticketsForUser)
            {
                i.Event = events.Find(o => o.ID == i.EventID).Name;
                i.Buyer = users.Find(o => o.ID == i.BuyerID).Nick;

            }
            return ticketsForUser;
        }

        public void update_order_info(int ticketID, int newStatus, string message = null, int sellerID = 0, int buyerID = 0)
        {
            Order order = Data.Orders.GetAll().Find(o => o.TicketID == ticketID);
            order.Status = (Status.status)newStatus;
            if (newStatus == 2 && message != null)
                order.TrackNumber = message;
            if (order.Status == (Status.status)2)
            {
                Ticket t = Data.Tickets.Get(ticketID);
                t.SellerID = buyerID;
                Data.Tickets.Update(t);
            }
            if (newStatus == 3)
            {
                TicketReject(ticketID);
                if (message != null)
                {
                    order.RejectionReason = message;
                }
            }
               
            Data.Orders.Update(order);
            Data.Save();
        }

        private void TicketReject(int ticketID) 
            {
                Ticket t = Data.Tickets.Get(ticketID);
                if (t != null)
                {
                    t.Availability = true;
                    Data.Tickets.Update(t);
                }
            } 
        #endregion

        #region userOrders
        public List<TicketDTO> TicketOrderUser(UserDTO user) //!!!
        {
            List<Order> ordersUser = Data.Orders.GetAll().FindAll(o => o.BuyerID == user.ID);
            List<User> users = Data.Users.GetAll();
            var allTickets = Tickets();
            List<TicketDTO> tickets = new List<TicketDTO>();
            foreach (var i in ordersUser)
            {
                int ti = i.TicketID;
                TicketDTO ticket = allTickets.Find(o => o.ID == ti);
                ticket.OrderStatus = i.Status.ToString();
                ticket.SellerID = i.SellerID;
                if(i.Status == (Status.status) 3 && i.RejectionReason != null)
                   ticket.AnswerText = i.RejectionReason;
                if (i.Status == (Status.status)2 && i.TrackNumber != null)
                    ticket.AnswerText = i.TrackNumber;
                tickets.Add(ticket);
            }
            List<Event> events = Data.Events.GetAll();
            foreach (var i in tickets)
            {
                i.Event = events.Find(o => o.ID == i.EventID).Name;
                i.Seller = users.Find(o => o.ID == i.SellerID).Nick;
            }
            return tickets;
        }
        #endregion

        public List<TicketDTO> TicketsForEvent(int eventID, decimal price)
        {
            List<TicketDTO> tickets = TicketsForEventWithoutSeller(eventID, price);
            List<User> users = Data.Users.GetAll();
            foreach (var i in tickets)
            {
                i.Seller = users.Find(o => o.ID == i.SellerID).FirstName;
            }
            return tickets;
        }
   
        public EventDTO GetEventForTicket(int eventId)
        {
            Event ev = Data.Events.GetAll().Find(o => o.ID == eventId);
            EventDTO e = Mapper.Map<EventDTO>(ev);
            e.City = Data.Cities.GetAll().Find(o => o.ID == e.CityID).Name;
            e.Venue = Data.Venues.GetAll().Find(o => o.ID == e.VenueId).Name;
            return e;
        }

        public TicketDTO GetTicket(int id)
        {
            return (Tickets().Find(o => o.ID == id));
        }

        private List<TicketDTO> Tickets() => Mapper.Map<List<TicketDTO>>(Data.Tickets.GetAll());

        private List<TicketDTO> TicketsForEventWithoutSeller(int eventID, decimal price) => (Tickets()
            .FindAll(o => (o.EventID == eventID) && (o.Availability) && (o.Price == price)));

    }
}
