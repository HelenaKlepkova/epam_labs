﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Hash;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UserService : IUserInfo
    {
        EFData data;
        public UserService(EFData data)
        {
            this.data = data;
        }
        public UserDTO GetUser(string nick)
        {
            List<User> users = data.Users.GetAll();
            return Mapper.Map<UserDTO>(users.Find(o => o.Nick == nick));
        }
        public void change_password(string nick, string newPassword)
        {
            User user = GetUserData(nick);
            Password psw = data.Passwords.GetAll().Where(o => o.UserID == user.ID).FirstOrDefault();
            psw.Password_ = Hash.HashPassword(newPassword);
            data.Passwords.Update(psw);
            data.Save();
        }

        public  void change_nick(string oldNick, string newNick)
        {
            User user = GetUserData(oldNick);
            user.Nick = newNick;
            data.Users.Update(user);
            data.Save();
        }

        public void change_phone(string nick, string newPhone)
        {
            User user = GetUserData(nick);
            user.PhoneNumber = newPhone;
            data.Users.Update(user);
            data.Save();
        }

        public void change_address(string nick, string newAddress)
        {
            User user = GetUserData(nick);
            user.Address = newAddress;
            data.Users.Update(user);
            data.Save();
        }

        private User GetUserData(string nick) => data.Users.GetAll().Where(o => o.Nick == nick).FirstOrDefault();
   
    }
}
