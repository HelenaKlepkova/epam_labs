﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class CityOptionService : ICityOption
    {
        EFData data;

        public CityOptionService(EFData data)
        {
            this.data = data;
        }

        #region cityOption

        public void create_city(CityDTO city)
        {
            data.Cities.Create(Mapper.Map<City>(city));
            data.Save();
        }

        public void delete_city(int cityID)
        {
            data.Cities.Delete(cityID);
            data.Save();
        }

        public CityDTO GetCity(int id) => Mapper.Map<CityDTO>(data.Cities.Get(id));

        public void update_city(CityDTO city)
        {
            City cityDB = data.Cities.Get(city.ID);
            if (cityDB != null)
            {
                cityDB.Name = city.Name;
                data.Cities.Update(cityDB);
                data.Save();
            }
        }

        #endregion

        public List<CityDTO> GetCities() => Mapper.Map<List<CityDTO>>(data.Cities.GetAll());
    }
}
