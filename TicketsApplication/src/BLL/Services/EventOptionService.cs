﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class EventOptionService : IEventOption
    {
        EFData data;

        public EventOptionService(EFData data)
        {
            this.data = data;
        }
        public List<CityDTO> GetCities() => Mapper.Map<List<CityDTO>>(data.Cities.GetAll());

        public List<VenueDTO> GetVenues() => Mapper.Map<List<VenueDTO>>(data.Venues.GetAll());
        public int GetIDLastEvent() => data.Events.GetAll().Count();

        public void add_event(EventDTO ev)
        {
            data.Events.Create(Mapper.Map<Event>(ev));
            data.Save();
        }
        public void add_tickets(int count, int eventID)
        {
            Random a = new Random();
            decimal[] prices = new decimal[5] { 100, 200, 300, 400, 500 };
            for (int i = 0; i < count; i++)
            {
                data.Tickets.Create(new Ticket { EventID = eventID, Price = prices[a.Next(0, 5)], SellerID = 1 });
            }
            data.Save();
        }

        public List<EventDTO> GetEvents(int cityID) => Mapper.Map<List<EventDTO>>(data.Events.GetAll()
            .Where(o => o.CityID == cityID).ToList());

        public void delete_event(int id)
        {
            data.Events.Delete(id);
            data.Save();
        }

        public string Banner(int id)
        {
            Event ev = data.Events.Get(id);
            return ev.Banner;
        }

        public EventDTO GetEvent(int id) => Mapper.Map<EventDTO>(data.Events.Get(id));

        public void update_event(EventDTO ev)
        {
            if (ev != null)
            {
                Event _event = data.Events.Get(ev.ID);
                if (_event != null)
                {
                    _event.Banner = ev.Banner;
                    _event.Date = ev.Date;
                    _event.CityID = ev.CityID;
                    _event.VenueId = ev.VenueId;
                    _event.Description = ev.Description;
                    _event.Name = ev.Name;
                    data.Events.Update(_event);
                    data.Save();
                }
            }
        }
    }
}
