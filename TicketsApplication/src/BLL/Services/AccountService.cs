﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class AccountService : IAccount
    {
        EFData data;
        public AccountService(EFData data)
        {
            this.data = data;
        }

        private List<UserDTO> Users() => Mapper.Map<List<UserDTO>>(data.Users.GetAll());
        private List<PasswordDTO> Passwords() => Mapper.Map<List<PasswordDTO>>(data.Passwords.GetAll());

        public int GetPasswordID(string password) => Passwords().Find(o => o.Password_ == password).ID;

        public int GetPasswordIdInUsers(string nick) => (data.Users.GetAll()).Find(o => o.Nick == nick).PasswordID;

        public string GetPassword(int id) => data.Passwords.Get(id).Password_;

        public int NewPasswordID()
        {
            List<Password> passwords = data.Passwords.GetAll();
            int result = passwords.Count() + 1;
            return result;
        }

        public bool IsExsists(string name) => Users().Exists(u => u.Nick == name);

        public string GetRoleNameForUserNick(string nick)
        {
            UserDTO user = GetUser(nick);
            string roleName= data.Roles.Get(user.RoleID).Name;
            return roleName;
        }

        public UserDTO GetUser(string nick)
        {
            List<UserDTO> users = Mapper.Map<List<UserDTO>>(data.Users.GetAll());
            UserDTO user = users.Find(o => (o.Nick == nick));
            return user;
        }

        public bool Register(UserDTO user, string password)
        {           
                User userForReg = Mapper.Map<User>(user);
                data.Users.Create(userForReg);
                data.Save();
                Password psw = new Password { Password_ = password };
                User newUser = data.Users.GetAll().Find(o => o.Nick == user.Nick);
                psw.UserID = newUser.ID;
                data.Passwords.Create(psw);
                data.Save();
                return true;          
        }

        public UserDTO CreateUser(string nick, string firstName, string lastName, int passwordId,
                                  string address, string localization, string phone)
        {
            UserDTO user = new UserDTO();
            user.Nick = nick;
            user.FirstName = firstName;
            user.LastName = lastName;
            user.PasswordID = passwordId;
            user.Address = address;
            user.PhoneNumber = phone;
            user.Localization = localization;
            return user;
        }
    }
}
