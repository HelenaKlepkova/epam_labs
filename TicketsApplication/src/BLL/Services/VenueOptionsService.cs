﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class VenueOptionsService : IVenueOptions
    {
        EFData data;

        public VenueOptionsService(EFData data)
        {
            this.data = data;
        }

        #region venueOption
        public void create_venue(VenueDTO venue)
        {
            if (venue != null)
            {
                data.Venues.Create(Mapper.Map<Venue>(venue));
                data.Save();
            }
        }

        public void delete_venue(int id)
        {
            data.Venues.Delete(id);
            data.Save();
        }

        public VenueDTO GetVenue(int id) => Mapper.Map<VenueDTO>(data.Venues.Get(id));

        public void update_venue(VenueDTO venue)
        {
            Venue _venue = data.Venues.Get(venue.ID);
            if (_venue != null)
            {
                _venue.Name = venue.Name;
                _venue.Address = venue.Address;
                data.Venues.Update(_venue);
                data.Save();
            }
        }

        #endregion

        public List<CityDTO> GetCities() => Mapper.Map<List<CityDTO>>(data.Cities.GetAll());
        public List<VenueDTO> GetVenues() => Mapper.Map<List<VenueDTO>>(data.Venues.GetAll());
    }
}
