﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class PasswordDTO
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Password_ { get; set; }
    }
}
