﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class RoleDTO
    {
        int ID { get; set; }
        string Name { get; set; }
    }
}
