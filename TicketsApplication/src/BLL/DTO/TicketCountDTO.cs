﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class TicketCountDTO
    {
        public ICollection<KeyValuePair<decimal, int>> PriceAndCount;
        public EventDTO Event { get; set; }
    }
}
