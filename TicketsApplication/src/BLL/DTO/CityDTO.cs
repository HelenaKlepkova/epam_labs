﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class CityDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
