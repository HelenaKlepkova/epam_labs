﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class TicketDTO
    {
        public int ID { get; set; }
        public int EventID { get; set; }
        public decimal Price { get; set; }
        public int SellerID { get; set; }
        public int BuyerID { get; set; }
        public bool Availability { get; set; }
        public string Seller { get; set; }
        public string Buyer { get; set; }
        public bool ForSale { get; set; }
        public string Event { get; set; }
        public string OrderStatus { get; set; }
        public string AnswerText { get; set; }
    }
}
