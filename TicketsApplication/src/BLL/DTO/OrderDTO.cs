﻿
namespace BLL.DTO
{
    public class OrderDTO
    {
        public int ID { get; set; }
        public int TicketID { get; set; }
        public bool Status { get; set; }
        public int BuyerID { get; set; }
        public string TrackNumber { get; set; }
        public string RejectionReason { get; set; }
        public int SellerID { get; set; }
    }
}
