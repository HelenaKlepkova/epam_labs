﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class EventDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public int VenueId { get; set; }
        public string Banner { get; set; }
        public string Description { get; set; }
        public int CityID { get; set; }
        public string Venue { get; set; }
        public string City { get; set; }

    }
}
