﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using DAL.EF;
using DAL.Statuses;

namespace TicketsApplication.Migrations
{
    [DbContext(typeof(DataContext))]
    partial class DataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DAL.Entities.City", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("Cities");
                });

            modelBuilder.Entity("DAL.Entities.Event", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Banner");

                    b.Property<int>("CityID");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<int>("VenueId");

                    b.HasKey("ID");

                    b.HasIndex("VenueId");

                    b.ToTable("Events");
                });

            modelBuilder.Entity("DAL.Entities.Order", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BuyerID");

                    b.Property<string>("RejectionReason");

                    b.Property<int>("SellerID");

                    b.Property<int>("Status");

                    b.Property<int>("TicketID");

                    b.Property<string>("TrackNumber");

                    b.HasKey("ID");

                    b.HasIndex("TicketID")
                        .IsUnique();

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("DAL.Entities.Password", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Password_");

                    b.Property<int>("UserID");

                    b.HasKey("ID");

                    b.ToTable("Passwords");
                });

            modelBuilder.Entity("DAL.Entities.Role", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("DAL.Entities.Ticket", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Availability");

                    b.Property<int>("BuyerID");

                    b.Property<int>("EventID");

                    b.Property<decimal>("Price");

                    b.Property<int>("SellerID");

                    b.Property<int?>("UserID");

                    b.HasKey("ID");

                    b.HasIndex("EventID");

                    b.HasIndex("UserID");

                    b.ToTable("Tickets");
                });

            modelBuilder.Entity("DAL.Entities.User", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<string>("FirstName")
                        .HasMaxLength(15);

                    b.Property<string>("LastName")
                        .HasMaxLength(30);

                    b.Property<string>("Localization");

                    b.Property<string>("Nick")
                        .HasMaxLength(15);

                    b.Property<int>("PasswordID");

                    b.Property<string>("PhoneNumber");

                    b.Property<int>("RoleID");

                    b.HasKey("ID");

                    b.HasIndex("RoleID");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("DAL.Entities.Venue", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<int>("CityID");

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.HasIndex("CityID");

                    b.ToTable("Venues");
                });

            modelBuilder.Entity("DAL.Entities.Event", b =>
                {
                    b.HasOne("DAL.Entities.Venue", "Venue")
                        .WithMany("Events")
                        .HasForeignKey("VenueId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DAL.Entities.Order", b =>
                {
                    b.HasOne("DAL.Entities.Ticket", "Ticket")
                        .WithOne("Order")
                        .HasForeignKey("DAL.Entities.Order", "TicketID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DAL.Entities.Ticket", b =>
                {
                    b.HasOne("DAL.Entities.Event", "Event")
                        .WithMany("Tickets")
                        .HasForeignKey("EventID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("DAL.Entities.User", "User")
                        .WithMany("Ticket")
                        .HasForeignKey("UserID");
                });

            modelBuilder.Entity("DAL.Entities.User", b =>
                {
                    b.HasOne("DAL.Entities.Role", "Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DAL.Entities.Venue", b =>
                {
                    b.HasOne("DAL.Entities.City", "City")
                        .WithMany("Venues")
                        .HasForeignKey("CityID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
