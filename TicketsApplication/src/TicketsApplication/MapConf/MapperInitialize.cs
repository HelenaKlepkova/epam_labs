﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using DAL.Entities;
using TicketsApplication.Models;

namespace TicketsApplication.MapConf
{
    public class MapperInitialize : Profile
    { 
        public MapperInitialize()
        {
            CreateMap<Password, PasswordDTO>();
            CreateMap<PasswordDTO, Password>();

            CreateMap<EventDTO, Event>()
                .ForMember("Venue", opt => opt.Ignore());

            CreateMap<Event, EventDTO>();
            CreateMap<EventVM, EventDTO>();
            CreateMap<EventDTO, EventVM>();
            CreateMap<EventDTO, EventUpdateVM>();
            CreateMap<EventUpdateVM, EventDTO>();

            CreateMap<UserDTO, User>();
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, UserVM>();
            CreateMap<UserVM, UserDTO>();

            CreateMap<CityDTO, City>();
            CreateMap<City, CityDTO>();
            CreateMap<CityDTO, CityVM>();
            CreateMap<CityVM, CityDTO>();

            CreateMap<OrderDTO, Order>();
            CreateMap<Order, OrderDTO>();

            CreateMap<TicketDTO, Ticket>();
            CreateMap<Ticket, TicketDTO>();
            CreateMap<TicketDTO, TicketVM>();
            CreateMap<TicketVM, TicketDTO>();
            CreateMap<TicketCountDTO, TicketCountVM>();
            CreateMap<TicketCountVM, TicketCountDTO>();

            CreateMap<VenueDTO, Venue>();
            CreateMap<Venue, VenueDTO>();
            CreateMap<VenueVM, VenueDTO>();
            CreateMap<VenueDTO, VenueVM>();
        }
    }
}
