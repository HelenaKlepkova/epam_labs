﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using BLL.Interfaces;
using AutoMapper;
using TicketsApplication.MapConf;
using BLL.DTO;
using TicketsApplication.Models;
using BLL.Services;
using Microsoft.AspNetCore.Mvc.Razor;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using DAL.EF;
using DAL.Repositories;


namespace TicketsApplication
{
    public class Startup
    {      
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = @"Server=(localdb)\mssqllocaldb;Database=ResaleDB_Alena_Klepkova;Trusted_Connection=True;";
            services.AddDbContext<DataContext>(options =>
               options.UseSqlServer(connection, b => b.MigrationsAssembly("TicketsApplication")));
            services.AddSingleton<EFData>();
            services.AddLocalization(opts => { opts.ResourcesPath = "Resources"; });
            services.AddMvc()
         .AddViewLocalization(
             LanguageViewLocationExpanderFormat.Suffix,
             opts => { opts.ResourcesPath = "Resources"; })
         .AddDataAnnotationsLocalization();

            services.Configure<RequestLocalizationOptions>(
                opts =>
                {
                    var supportedCultures = new[]
                    {
                        new CultureInfo("en"),
                        new CultureInfo("ru"),
                        new CultureInfo("be")
                    };

                    opts.DefaultRequestCulture = new RequestCulture("en");
                    opts.SupportedCultures = supportedCultures;
                    opts.SupportedUICultures = supportedCultures;
                });
            services.AddTransient<IEventService, EventService>();
            services.AddTransient<IAccount, AccountService>();
            services.AddTransient<ITicketService, TicketService>();
            services.AddTransient<IUserInfo, UserService>();
            services.AddTransient<IAdmin, AdminService>();
            services.AddTransient<IVenueOptions, VenueOptionsService>();
            services.AddTransient<ICityOption, CityOptionService>();
            services.AddTransient<IEventOption, EventOptionService>();
            services.AddDistributedMemoryCache();
            services.AddSession();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);
            Mapper.Initialize(c =>
            {
                c.AddProfile<MapperInitialize>();
            });


            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {

                AuthenticationScheme = "Cookies",
                LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login"),
                AutomaticAuthenticate = true,
                AutomaticChallenge = true
            });

         

            app.UseStaticFiles();
            app.UseSession();
            SampleData.Initialize(app.ApplicationServices);
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

            });

          
        }
    }
}
