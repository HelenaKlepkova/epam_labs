﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TicketsApplication.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Unknown FirstName")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Unknown LastName")]
        public string LastName { get; set; }

        [Required(ErrorMessage ="Unknown nick")]
        public string Nick { get; set; }

        [Required(ErrorMessage = "Unknown Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Unknown Phone")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Unknown Address")]
        public string Address { get; set; }

        public string Localization { get; set; }
    }
}
