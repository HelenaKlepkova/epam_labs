﻿using DAL.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using DAL.Entities;
using DAL.Statuses;

namespace TicketsApplication.Models
{
    public static class SampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<DataContext>();

            if (!(context.Cities.Any() && context.Events.Any() && context.Venues.Any() && context.Users.Any()))
            {
                context.Roles.AddRange(
                    new Role { Name="Admin"},
                    new Role { Name="User"}
                    );
                context.SaveChanges();
                context.Passwords.AddRange(
                   new Password { Password_ = "AKQHgxN2kYQL6dRQrt4hrdrWLyIx/+EIKTvMJNc1bxxh6zku05YwCVqG52JN0fADUQ==", UserID = 1 },
                   new Password { Password_ = "AF0mgIfAwXlIj0hMguBwLlzojfV9ouWs84DAeBHo4Lo1hEggYf7Y3DbRGD42m1QRJA==", UserID = 2 }
                   );
                context.SaveChanges();

                context.Users.AddRange(
                    new User { Nick = "Admin", FirstName = "AdminF", LastName = "AdminL", PhoneNumber = "23-23-23", Address = "Gomel, Oskina 1", Localization = "ru", PasswordID = 1, RoleID = 1 },
                    new User { Nick = "User", FirstName = "UserF", LastName = "UserL", Localization = "ru", Address = "Gomel, Oskina 1", PhoneNumber = "23-23-23", PasswordID = 2, RoleID = 2 }
                    );
                context.SaveChanges();
               
                context.Cities.AddRange(
                    new City { Name = "Gomel" },
                    new City { Name = "Minsk" },
                    new City { Name = "Grodno" },
                    new City { Name = "Brest" },
                    new City { Name = "Vitebsk" },
                    new City { Name = "Mogilev" }
                    );
                context.SaveChanges();
                context.Venues.AddRange(
              new Venue { CityID = 1, Name = "Palace Of Culture", Address = "Lenina 1" },
              new Venue { CityID = 1, Name = "City Culture Center", Address = "Irininskaya 35" },
              new Venue { CityID = 2, Name = "Palace Of Culture", Address = "Bogdanovicha 41" },
              new Venue { CityID = 2, Name = "City Culture Center", Address = "Kolasa 46" },
              new Venue { CityID = 3, Name = "Palace Of Culture", Address = "Rabkorovskaya 34" },
              new Venue { CityID = 3, Name = "City Culture Center", Address = "Sovetskaya 4" },
              new Venue { CityID = 4, Name = "Palace Of Culture", Address = "Lange 3" },
              new Venue { CityID = 4, Name = "City Culture Center", Address = "Volgogradskya 23" },
              new Venue { CityID = 5, Name = "Palace Of Culture", Address = "Karpovicha 4" },
              new Venue { CityID = 5, Name = "City Culture Center", Address = "Tvena 56" },
              new Venue { CityID = 6, Name = "Palace Of Culture", Address = "Ryabinovaya 149" },
              new Venue { CityID = 6, Name = "City Culture Center", Address = "Belyaeva 48" }
                );
                context.SaveChanges();
                context.Events.AddRange(
                new Event { Date = new DateTime(2017, 12, 1), Description = @"Christina Orbakaite will present a new show program on December 1 Gomel. The concert with the intriguing title ""Masks"" will be held at the Great Hall of the Palace Culture!", Name = "Christina Orbakaite", VenueId = 1, CityID = 1, Banner = "images/1.jpg" },
                new Event { Date = new DateTime(2017, 12, 2), Description = "Boris Grebenshchikov continues to please fans of unusual forms of their performances.Being in constant motion, in the continuing search, experimenting with different styles and genres of music, he always retains the ability to please and surprise the new and unexpected.", Name = "Aquarium-Band concert ", VenueId = 2, CityID = 1, Banner = "images/2.jpg" },
                new Event { Date = new DateTime(2017, 12, 3), Description = "Equally recognizable social texts, words about freedom, about dreams, about love and fight to the last drop of blood - an honest and uncompromising songs of the group favorite huge number of fans all over the country, will be performed on December 3 in Minsk Palace of Culture.", Name = "Concert lumen", VenueId = 3, CityID = 2, Banner = "images/3.jpg" },
                new Event { Date = new DateTime(2017, 12, 4), Description = @"The new program Grigory Leps: actor arrives in Minsk. Of course, not is absolutely no problem for this musician, ""to collect the stadium"" once again.At this time in the Belarusian capital", Name = "Grigory Leps concert", VenueId = 4, CityID = 2, Banner = "images/4.jpg" },
                new Event { Date = new DateTime(2017, 12, 4), Description = "Grodno Nickelback fans will be surprised by light and sound show.Besides the new songs will sound and the already known compositions.Hurry to buy tickets now with the approaching date of the concert cost will increase", Name = "Nickelback concert", VenueId = 5, CityID = 3, Banner = "images/5.jpg" },
                new Event { Date = new DateTime(2017, 12, 5), Description = "DiDjuLja - guitar virtuoso, composer and leader of the eponymous instrumental group that plays music in the style range from neo-flamenco to rock and folk.", Name = "Didulja concert", VenueId = 11, CityID = 6, Banner = "images/6.jpg" }
                    );
                context.SaveChanges();
                Random a = new Random();
                int[] prices = new int[5] { 100, 200, 300, 400, 500 };
                List<int> tickets = new List<int> { 4, 5, 47, 12, 46, 41, 7, 40, 39, 48 };
                for (int i = 1; i <= 100; i++)
                {
                    if (tickets.Contains(i))
                    {
                        context.Tickets.Add(new Ticket { EventID = a.Next(1, 4), Price = prices[a.Next(0, 5)], SellerID = 1, Availability = false });
                    }
                    else
                    {
                        context.Tickets.Add(new Ticket { EventID = a.Next(1, 4), Price = prices[a.Next(0, 5)], SellerID = 1, Availability = true });
                    }
                    context.SaveChanges();
                }
             
                context.Orders.AddRange(
                new Order { BuyerID = 1, Status = (Status.status)1, SellerID = 2, TicketID = 4 },
                new Order { BuyerID = 2, Status = (Status.status)2, TrackNumber = "RA123456789RU", SellerID = 1, TicketID = 5 },
                new Order { BuyerID = 1, Status = (Status.status)2, TrackNumber = "RA123456789RU", SellerID = 2, TicketID = 47 },
                new Order { BuyerID = 2, Status = (Status.status)2, TrackNumber = "RA123456789RU", SellerID = 1, TicketID = 12 },
                new Order { BuyerID = 2, Status = (Status.status)1, SellerID = 1, TicketID = 46 },
                new Order { BuyerID = 1, Status = (Status.status)2, TrackNumber = "RA123456789RU", SellerID = 2, TicketID = 41 },
                new Order { BuyerID = 1, Status = (Status.status)2, TrackNumber = "RA123456789RU", SellerID = 2, TicketID = 7 },
                new Order { BuyerID = 1, Status = (Status.status)1, SellerID = 2, TicketID = 40 },
                new Order { BuyerID = 2, Status = (Status.status)2, TrackNumber = "RA123456789RU", SellerID = 1, TicketID = 39 },
                new Order { BuyerID = 1, Status = (Status.status)2, TrackNumber = "RA123456789RU", SellerID = 2, TicketID = 48 }
                    );
                context.SaveChanges();
            }

        }
    }
}