﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TicketsApplication.Models
{
    public class EventVM
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public int VenueID { get; set; }
        public string Banner { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int CityID { get; set; }
        public string Venue { get; set; }
        public string City { get; set; }
        public List<CityVM> Cities { get; set; }
        public List<VenueVM> Venues { get; set; }
 
    }
}
