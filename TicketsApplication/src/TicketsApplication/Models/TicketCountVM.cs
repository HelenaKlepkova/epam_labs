﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DTO;

namespace TicketsApplication.Models
{
    public class TicketCountVM
    {
        public ICollection<KeyValuePair<decimal, int>> PriceAndCount;
        public EventVM Event { get; set; }
        public decimal SelectPrice { get; set; }
    }
}
