﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplication.Models;

namespace TicketsApplication.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EventOptionsController : Controller
    {
        IHostingEnvironment env;
        IEventOption serv;

        public EventOptionsController(IHostingEnvironment env, IEventOption serv)
        {
            this.env = env;
            this.serv = serv;
        }

        #region createEvent

        [HttpGet]
        public IActionResult create_event()
        {
            EventVM model = new EventVM();
            model.Date = DateTime.Now;
            int selectedIndex = 1;
            model.Cities = Mapper.Map<List<CityVM>>(serv.GetCities());
            SelectList cities = new SelectList(model.Cities, "ID", "Name", selectedIndex);
            ViewBag.Cities = cities;
            model.Venues = Mapper.Map<List<VenueVM>>(serv.GetVenues());
            SelectList venues = new SelectList(model.Venues.Where(c => c.CityID == selectedIndex), "ID", "Name");
            ViewBag.Venues = venues;
            return View(model);
        }

        [HttpGet]
        public PartialViewResult GetVenues(int id)
        {
            return PartialView(serv.GetVenues().Where(c => c.CityID == id).ToList());
        }

        [HttpPost]
        public IActionResult create_event(EventVM model, IFormFile downloadableFile, int countTickets)
        {
            if (ModelState.IsValid)
            {
                if (model.Date > DateTime.Now)
                {
                    if (downloadableFile != null)
                    {
                        using (var fileStream = new FileStream(env.WebRootPath + "/images/" + downloadableFile.FileName, FileMode.Create))
                        {
                            downloadableFile.CopyTo(fileStream);
                        }
                        model.Banner = "images/" + downloadableFile.FileName;
                    }
                    serv.add_event(Mapper.Map<EventDTO>(model));
                    int eventID = serv.GetIDLastEvent();
                    serv.add_tickets(countTickets, eventID);
                }
                else
                {
                    return RedirectToAction("createEvent");
                }
            }
            else
            {
                return RedirectToAction("createEvent");
            }

            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region deleteEvent

        [HttpGet]
        public IActionResult delete_event()
        {
            return View(DataSelection());
        }

        [HttpPost]
        public IActionResult delete_event(EventVM model)
        {
            if (model.ID != 0)
            {
                string path1 = serv.Banner(model.ID);
                string path2 = env.WebRootPath + "/" + path1;
                string path = path2.Replace("/", @"\");
                try
                {
                    System.IO.File.Delete(path);
                }
                catch
                { }

                serv.delete_event(model.ID);
            }
            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region updateEvent

        [HttpGet]
        public IActionResult update_event()
        {
            return View(DataSelection());
        }

        [HttpPost]
        public IActionResult update_event_action(int id)
        {
            EventUpdateVM vm = Mapper.Map<EventUpdateVM>(serv.GetEvent(id));
            int selectedIndex = vm.CityID;
            vm.Cities = Mapper.Map<List<CityVM>>(serv.GetCities());
            SelectList cities = new SelectList(vm.Cities, "ID", "Name", selectedIndex);
            ViewBag.Cities = cities;
            vm.Venues = Mapper.Map<List<VenueVM>>(serv.GetVenues());
            SelectList venues = new SelectList(vm.Venues.Where(c => c.CityID == selectedIndex), "ID", "Name");
            ViewBag.Venues = venues;
            return View(vm);
        }
        [HttpPost]
        public IActionResult update_event_result(EventUpdateVM model)
        {

            EventUpdateVM oldModel = Mapper.Map<EventUpdateVM>(serv.GetEvent(model.ID));
            if (ModelState.IsValid)
            {
                model.Banner = oldModel.Banner;
                model.Date = oldModel.Date;
                if (model.Name == null)
                    model.Name = oldModel.Name;
                if (model.Description == null)
                    model.Description = oldModel.Description;

                serv.update_event(Mapper.Map<EventDTO>(model));
                return RedirectToAction("Index", "Home");
            }
            else
                return RedirectToAction("update_event_result");
        }
        #endregion

        [HttpGet]
        public PartialViewResult GetEvents(int id)
        {
            return PartialView(serv.GetEvents(id));
        }

        private EventVM DataSelection()
        {
            EventVM model = new EventVM();
            int selectedIndex = 1;
            List<CityVM> cities = Mapper.Map<List<CityVM>>(serv.GetCities());
            SelectList citiesDD = new SelectList(cities, "ID", "Name", selectedIndex);
            List<EventVM> events = Mapper.Map<List<EventVM>>(serv.GetEvents(selectedIndex));
            SelectList eventsDD = new SelectList(events, "ID", "Name");
            ViewBag.Cities = cities;
            ViewBag.Events = events;
            return model;
        }
    }
}
