﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplication.Models;

namespace TicketsApplication.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        IAdmin serv;
        IHostingEnvironment env;
        public AdminController(IAdmin serv, IHostingEnvironment env)
        {
            this.serv = serv;
            this.env = env;
        }

        public IActionResult AdminOptions()
        {
            return View();
        }   

        #region addAdmin

        [HttpGet]
        public IActionResult optionAddOrDeleteAdmin()
        {
            List<UserVM> users = Mapper.Map<List<UserVM>>(serv.GetUsers().Where(o => o.RoleID == 2));
            if (users.Count != 0)
            {
                int selectedIndex = users[0].ID;
                SelectList usersSL = new SelectList(users, "ID", "Name", selectedIndex);
                ViewBag.Users = users;
                return View();
            }
            return View();
        }
        [HttpGet]
        public PartialViewResult GetAdmins()
        {
            List<UserVM> users = Mapper.Map<List<UserVM>>(serv.GetUsers().Where(o => o.RoleID == 1));
            if (users.Count != 0)
            {
                int selectedIndex = users[0].ID;
                SelectList usersSL = new SelectList(users, "ID", "Name", selectedIndex);
                ViewBag.Users = users;
            }
            return PartialView();
        }
        [HttpGet]
        public PartialViewResult GetUsers()
        {
            List<UserVM> users = Mapper.Map<List<UserVM>>(serv.GetUsers().Where(o => o.RoleID == 2));
            if (users.Count != 0)
            {
                int selectedIndex = users[0].ID;
                SelectList usersSL = new SelectList(users, "ID", "Name", selectedIndex);
                ViewBag.Users = users;
            }
            return PartialView();
        }

        [HttpPost]
        public IActionResult optionAddOrDeleteAdmin(UserVM model, string roleTarget)
        {
            serv.change_role(model.ID, roleTarget);
            return RedirectToAction("Index", "Home");
        } 
        #endregion
    }
}
