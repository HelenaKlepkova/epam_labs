﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplication.Models;

namespace TicketsApplication.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CitiesController : Controller
    {
        ICityOption serv;
        public CitiesController(ICityOption serv)
        {
            this.serv = serv;
        }

        #region createCity

        [HttpGet]
        public IActionResult create_city()
        {
            return View();
        }
        [HttpPost]
        public IActionResult create_city(CityVM model)
        {
            if (ModelState.IsValid)
            {
                serv.create_city(Mapper.Map<CityDTO>(model));
                return RedirectToAction("index", "Home");
            }
            return RedirectToAction("createCity");

        }
        #endregion

        #region deleteCity
        [HttpGet]
        public IActionResult delete_city()
        {
            int selectedIndex = 1;
            List<CityVM> citiesL = Mapper.Map<List<CityVM>>(serv.GetCities());
            SelectList cities = new SelectList(citiesL, "ID", "Name", selectedIndex);
            ViewBag.Cities = cities;
            return View();
        }
        [HttpPost]
        public IActionResult delete_city(int CityID)
        {
            serv.delete_city(CityID);
            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region updateCity
        [HttpGet]
        public IActionResult update_city()
        {
            int selectedIndex = 1;
            List<CityVM> citiesL = Mapper.Map<List<CityVM>>(serv.GetCities());
            SelectList cities = new SelectList(citiesL, "ID", "Name", selectedIndex);
            ViewBag.Cities = cities;
            return View();
        }
        [HttpPost]
        public IActionResult update_city_action(int cityID)
        {
            CityVM vm = Mapper.Map<CityVM>(serv.GetCity(cityID));
            return View(vm);
        }

        [HttpPost]
        public IActionResult update_city_result(CityVM model)
        {
            if (ModelState.IsValid)
            {
                serv.update_city(Mapper.Map<CityDTO>(model));
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("updateCityResult");
        }
        #endregion
    }
}
