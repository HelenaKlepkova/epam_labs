﻿using BLL.DTO;
using BLL.Interfaces;
using DAL.Hash;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TicketsApplication.Models;


namespace TicketsApplication.Controllers
{
    public class AccountController : Controller
    {
        IAccount accountServ;
        public AccountController(IAccount accountServ)
        {
            this.accountServ = accountServ;
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                if (!accountServ.IsExsists(model.Nick))
                {
                    int psw = accountServ.NewPasswordID();
                    accountServ.Register(new UserDTO
                    {
                        Nick = model.Nick,
                        FirstName = model.FirstName,
                        PasswordID = psw,
                        LastName = model.LastName,
                        PhoneNumber = model.PhoneNumber,
                        Address = model.Address,
                        Localization = model.Localization,
                        RoleID = 2
                    }, model.Password);
                    await Authenticate(model.Nick);
                    return RedirectToAction("Index", "Home");
                }
                else
                    ModelState.AddModelError("", "Incorrect username and(or) password");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                int psw = accountServ.GetPasswordIdInUsers(model.Nick);
                string password = accountServ.GetPassword(psw);
                if (Hash.VerifyHashedPassword(password, model.Password))
                {
                    UserVM user = new UserVM { Nick = model.Nick, PasswordID = psw };
                    if (user != null)
                    {
                        await Authenticate(user.Nick);

                        return RedirectToAction("Index", "Home");
                    }
                    ModelState.AddModelError("", "Incorrect username and(or) password");
                }
            }
            return View(model);
        }

        private async Task Authenticate(string userNick)
        {
            var claims = new List<Claim>
                   {
                        new Claim(ClaimsIdentity.DefaultNameClaimType, userNick),
                        new Claim(ClaimsIdentity.DefaultRoleClaimType, accountServ.GetRoleNameForUserNick(userNick))
                   };

            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.Authentication.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("Cookies");
            return RedirectToAction("Index", "Home");
        }
    }
}
