﻿using AutoMapper;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplication.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Principal;
using System.Security.Claims;

namespace TicketsApplication.Controllers
{
    public class UserController : Controller
    {
        IUserInfo us;
        IAccount accServ;
        public UserController(IUserInfo us, IAccount accServ)
        {
            this.us = us;
            this.accServ = accServ;
        }

        public IActionResult UserInformation(string nick)
        {
            UserVM user = Mapper.Map<UserVM>(us.GetUser(nick));
            return View(user);
        }
        public IActionResult AccountOptions()
        {
            UserVM user = Mapper.Map<UserVM>(us.GetUser(User.Identity.Name));
            return View(user);
        }

      
        public IActionResult changeAddress()
        {
            return PartialView();
        }
        public IActionResult changePhone()
        {
            return PartialView();
        }
        [HttpPost]
        public IActionResult changePhone(string newPhone)
        {
            if (newPhone != null)
            {
                us.change_phone(User.Identity.Name, newPhone);
            }
            return RedirectToAction("AccountOptions");
        }
        [HttpPost]
        public IActionResult changeAddress(string newAddress)
        {
            if (newAddress != null)
            {
                us.change_address(User.Identity.Name, newAddress);
            }
            return RedirectToAction("AccountOptions");
        }
   
        public IActionResult changePassword()
        {
            return PartialView();
        }
        [HttpPost]
        public IActionResult changePassword(string newPassword)
        {
            if (newPassword != null)
            {
                us.change_password(User.Identity.Name, newPassword);
            }
          
            return RedirectToAction("AccountOptions");
        }

        private async Task Authenticate(string userNick)
        {
            var claims = new List<Claim>
                   {
                        new Claim(ClaimsIdentity.DefaultNameClaimType, userNick),
                        new Claim(ClaimsIdentity.DefaultRoleClaimType, accServ.GetRoleNameForUserNick(userNick))
                   };

            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.Authentication.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }

    }
}
