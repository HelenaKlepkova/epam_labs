﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplication.Models;

namespace TicketsApplication.Controllers
{
    [Authorize]
    public class TicketController : Controller
    {
            ITicketService ts;
            IUserInfo us;

            public TicketController(ITicketService ts, IUserInfo us)
            {
                this.ts = ts;
                this.us = us;
            }
        #region level1 Отпрака собственных билетов на продажу
        public IActionResult TicketsStart()
        {
            List<TicketVM> model = Mapper.Map<List<TicketVM>>(ts.TicketStart(us.GetUser(User.Identity.Name)));
            return View(model);
        }

        [HttpPost]
        public IActionResult ticketForSale(List<TicketVM> model)
        {
            List<TicketVM> result = new List<TicketVM>();
            foreach (var i in model)
            {
                if (i.Availability)
                    result.Add(i);
            }
            ts.update_tickets(Mapper.Map<List<TicketDTO>>(result));
            return RedirectToAction("TicketsStart");
        }
        #endregion

        #region level2 продающиеся билеты
        [AllowAnonymous]
        public IActionResult Tickets(int id)
        {
            TicketCountVM tcvm = new TicketCountVM();
            tcvm.PriceAndCount = Mapper.Map<ICollection<KeyValuePair<decimal, int>>>(ts.GetTicketCountDTO(id));
            EventDTO ev = ts.GetEventForTicket(id);
            tcvm.Event = Mapper.Map<EventVM>(ev);
            return View(tcvm);
        }

        #endregion

        #region level3 покупка
        public IActionResult TicketsForOrder(decimal price, int eventId)
        {
            List<TicketDTO> tdto = ts.TicketsForEvent(eventId, price);
            List<TicketVM> tickets = Mapper.Map<List<TicketVM>>(tdto);
            return View(tickets);
        }

        public IActionResult Buy(List<TicketVM> models)
        {
            int userID = us.GetUser(User.Identity.Name).ID;
            List<TicketVM> result = new List<TicketVM>();
            foreach (var i in models)
            {
                if (i.ForSale)
                {
                    i.BuyerID = userID;
                    i.Availability = false;
                    result.Add(i);
                }
            }
            ts.buy_tickets(Mapper.Map<List<TicketDTO>>(result));
            return RedirectToAction("UserOrders");
        }

        #endregion

        #region level4 заказы у пользователя

        public IActionResult UserTickets()
        {
            List<TicketVM> tickets = Mapper
                .Map<List<TicketVM>>(ts.TicketsForUser(us.GetUser(User.Identity.Name)));
            return View(tickets);
        }

        [HttpPost]
        public IActionResult change_order_status(List<TicketVM> model)
        {
            if (model.Count != 0)
            {
                foreach(var i in model)
                {
                    if (i.OrderStatus == "sold")
                    {
                        ts.update_order_info(i.ID, 2, i.AnswerText, i.SellerID, i.BuyerID);
                    }
                    if (i.OrderStatus == "reject")
                    {
                        ts.update_order_info(i.ID, 3, i.AnswerText);
                    }
                }
            }
            return RedirectToAction("UserTickets");
        }
        #endregion

        public IActionResult UserOrders()
            {
                List<TicketVM> tickets = Mapper
                .Map<List<TicketVM>>(ts.TicketOrderUser(us.GetUser(User.Identity.Name)));
                return View(tickets);
            }
    }
}
