﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplication.Models;

namespace TicketsApplication.Controllers
{
    public class VenueOptionsController : Controller
    {
        IVenueOptions serv;

        public VenueOptionsController(IVenueOptions serv)
        {
            this.serv = serv;
        }

        #region createVenue

        [HttpGet]
        public IActionResult create_venue()
        {
            int selectedIndex = 1;
            List<CityVM> cities = Mapper.Map<List<CityVM>>(serv.GetCities());
            SelectList citiesDD = new SelectList(cities, "ID", "Name", selectedIndex);
            ViewBag.Cities = cities;
            return View();
        }
        [HttpPost]
        public IActionResult create_venue(VenueVM model)
        {
            if (ModelState.IsValid)
            {
                serv.create_venue(Mapper.Map<VenueDTO>(model));
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("createVenue");
        }
        #endregion

        #region deleteVenue
        [HttpGet]
        public IActionResult delete_venue()
        {
            int selectedIndex = 1;
            List<CityVM> cityL = Mapper.Map<List<CityVM>>(serv.GetCities());
            SelectList cities = new SelectList(cityL, "ID", "Name", selectedIndex);
            ViewBag.Cities = cities;
            List<VenueVM> venuesL = Mapper.Map<List<VenueVM>>(serv.GetVenues());
            SelectList venues = new SelectList(venuesL.Where(c => c.CityID == selectedIndex), "ID", "Name");
            ViewBag.Venues = venues;
            return View();
        }
        [HttpPost]
        public IActionResult delete_venue(VenueVM model)
        {
            if (model != null && model.ID != 0)
            {
                serv.delete_venue(model.ID);
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("deleteVenue");
        }
        #endregion

        #region updateVenue
        [HttpGet]
        public IActionResult update_venue()
        {
            int selectedIndex = 1;
            List<CityVM> cityL = Mapper.Map<List<CityVM>>(serv.GetCities());
            SelectList cities = new SelectList(cityL, "ID", "Name", selectedIndex);
            ViewBag.Cities = cities;
            List<VenueVM> venuesL = Mapper.Map<List<VenueVM>>(serv.GetVenues());
            SelectList venues = new SelectList(venuesL.Where(c => c.CityID == selectedIndex), "ID", "Name");
            ViewBag.Venues = venues;
            return View();
        }

        [HttpGet]
        public PartialViewResult GetVenues(int id)
        {
            return PartialView(serv.GetVenues().Where(c => c.CityID == id).ToList());
        }

        [HttpGet]
        public PartialViewResult VenuesGet(int id)
        {
            return PartialView(serv.GetVenues().Where(c => c.CityID == id).ToList());
        }
        [HttpPost]
        public IActionResult update_venue_action(int id)
        {
            VenueVM vm = Mapper.Map<VenueVM>(serv.GetVenue(id));
            return View(vm);

        }

        [HttpPost]
        public IActionResult update_venue_result(VenueVM model)
        {
            if (ModelState.IsValid)
            {
                serv.update_venue(Mapper.Map<VenueDTO>(model));
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("updateVenueResult");
        }
        #endregion
    }
}
