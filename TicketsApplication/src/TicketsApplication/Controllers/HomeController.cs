﻿using System;
using System.Collections.Generic;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using TicketsApplication.Models;
using BLL.DTO;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;


namespace TicketsApplication.Controllers
{
    public class HomeController : Controller
    {
       IEventService eventService;
        public HomeController(IEventService serv)
        {
            eventService = serv;
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }

        private List<EventVM> Events()
        {
            IEnumerable<EventDTO> eventDtos = eventService.GetEventsWithCity();
            return Mapper.Map<List<EventVM>>(eventDtos);
        }

        public IActionResult Index()
        {           
            return View(Events());
        }
    }
}
